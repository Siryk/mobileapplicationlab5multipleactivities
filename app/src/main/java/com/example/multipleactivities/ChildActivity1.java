package com.example.multipleactivities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class ChildActivity1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_activity1);
    }
}