package com.example.multipleactivities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class ChildActivity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_activity3);
    }
}