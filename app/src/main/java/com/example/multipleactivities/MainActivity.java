package com.example.multipleactivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView image1 = findViewById(R.id.image1);
        ImageView image2 = findViewById(R.id.image2);
        ImageView image3 = findViewById(R.id.image3);

        image1.setOnClickListener(this);
        image2.setOnClickListener(this);
        image3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.image1:
                intent = new Intent(MainActivity.this, ChildActivity1.class);
                startActivity(intent);
                break;
            case R.id.image2:
                intent = new Intent(MainActivity.this, ChildActivity2.class);
                startActivity(intent);
                break;
            case R.id.image3:
                intent = new Intent(MainActivity.this, ChildActivity3.class);
                startActivity(intent);
                break;
        }
    }
}